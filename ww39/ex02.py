Exercise 2:
 Rewrite your pay program using try and except so that your
 program handles non-numeric input gracefully by printing a message
 and exiting the program. The following shows two executions of the
 program:
     
Enter Hours: 20
Enter Rate: nine
Error, please enter numeric input
Enter Hours: forty
Error, please enter numeric input

hours= input("enter hours")
rate= input("enter pay rate")

if hours>40
pay=40* rate
bonus=((hours - 40) * rate) * 1.5
pay=bonus + pay

else:
    pay=hours*rate
    print(pay)
except:
    print("please enter numeric")
