#Exercise 2: Write another program that prompts for a list of numbers as above and at the end prints out both the
# maximum and minimum of the numbers instead of the average.

count = 0

while True:
    string_value = input("Enter a number: ")
    if string_value == 'done':
        break
    try:
        value = float(string_value)
    except:
        print("Invalid number")
        continue
    if count == 0:
        min_value = value
        max_value = value
    elif value < min_value:
        min_value = value
    elif value > max_value:
        max_value = value
    count = count + 1
    print(min_value, max_value)


