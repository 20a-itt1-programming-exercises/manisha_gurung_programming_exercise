#Exercise 3: Write a program that reads a file and prints the letters in decreasing order of frequency.
# Your program should convert all the input to lower case and only count the letters a-z. Your program should
#not count spaces, digits, punctuation, or anything other than the letters a-z. Find text samples from several
# different languages and see how letter frequency varies between languages. Compare your results with
#the tables at https://wikipedia.org/wiki/Letter_frequencies.

alphabet = 'abcdefghijklmnopqrstuvwxyz'
#print(len(alphabet))

filename = input("Enter a file name: ")
fhand = open(filename, 'r')
text = fhand.read()

#for ch in text:
#print(ch)      #comments: print out every single words one by one

freq_dict = {}
for ch in text.lower():
    if ch in alphabet:
        freq_dict[ch] = freq_dict.get(ch, 0) +1

lst = []
for key, value in freq_dict.items():
    lst.append((value,key))

lst.sort(reverse=True)
for freq,letter in lst:
    print(letter,freq)
